import { Component, HostListener, OnInit } from '@angular/core';
import { Bootstrap } from 'bootstrap/dist/js/bootstrap';
import { Popover } from 'bootstrap';
import { NotificationService } from './Services/Notifcations/notification.service';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import * as fromRoot from "./app.reducer";
import * as fromUser from "./Reducers/User/User.reducer"
import { UserService } from './Services/User/user.service';
import { Notification } from './Models/Notification/notification'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'ProjectManager';
  collapsed = true;

  ActiveUserName$ : Observable<string>;
  IsUserLoggedIn$ : Observable<boolean>;

  NotificationList : Notification[] = [];
  NotificationSubscription : Subscription;

  constructor(
    private notificationService : NotificationService,
    private UserState : Store<fromUser.UserState>,
    private userService : UserService
  ){

  }

  ngOnInit(){

    this.ActiveUserName$ = this.UserState.select( fromRoot.GetUserName );
    this.IsUserLoggedIn$ = this.UserState.select(fromRoot.GetIsUserLoggedIn);

  }

  isUserLoggedIn() : boolean{
    this.IsUserLoggedIn$.pipe(take(1)).subscribe((loggedIn) => {
      if (loggedIn == undefined) {
        return false;
      }else{
        return true;
      }
    });
    return false;
  }

  ngAfterViewInit(){
    this.NotificationSubscription = this.notificationService.Notification$
    .subscribe( (notification) => {
      this.NotificationList.push(notification)
    })
  }



  // @HostListener('window:beforeunload', ['$event'])
  // unloadHandler(event: Event) {
  //   window.opener.location.reload();
  // }

  LogOut(){

    this.userService.Logout();

  }

}
