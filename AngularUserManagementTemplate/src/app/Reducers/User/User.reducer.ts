import { Action } from '@ngrx/store';

import {
  UserActions,
  SET_USER_NAME,
  SET_USER_ROLE  ,
  SET_USER_TOKEN ,
  SET_USER_LOGGED_IN,
  SET_USER_ID
} from './user.actions';

export interface UserState {
  UserId : string;
  UserName: string;
  UserRole: string;
  UserToken: string;
  IsUserLoggedIn: boolean;
}

const initialState: UserState = {
  UserId: '',
  UserName: '',
  UserRole: '',
  UserToken: '',
  IsUserLoggedIn : false
};

export function UserReducer(state = initialState, action: UserActions) {
  switch (action.type) {
    case SET_USER_ID:
      return {
        ...state,
        UserId: action.payload,
      };
    case SET_USER_NAME:
      return {
        ...state,
        UserName: action.payload,
      };
    case SET_USER_ROLE:
      return {
        ...state,
        UserRole: action.payload,
      };
      case SET_USER_TOKEN:
        return {
          ...state,
          UserToken: action.payload,
        };
      case SET_USER_LOGGED_IN:
        return {
          ...state,
          IsUserLoggedIn: action.payload,
        };
    default: {
      return state;
    }
  }
}

export const GetUserId = (state: UserState) => state.UserId;
export const GetUserName = (state: UserState) => state.UserName;
export const GetUserRole = (state: UserState) => state.UserRole;
export const GetUserToken = (state: UserState) => state.UserToken;
export const GetIsUserLoggedIn = (state: UserState) => state.IsUserLoggedIn;
