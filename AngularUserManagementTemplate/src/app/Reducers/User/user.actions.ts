import { Action } from '@ngrx/store';

export const SET_USER_NAME = '[User] Set UserName';
export const SET_USER_ROLE = '[User] Set UserRole';
export const SET_USER_TOKEN = '[User] Set UserToken';
export const SET_USER_LOGGED_IN = '[User] Set IsUserLogggedIn';
export const SET_USER_ID = '[User] Set UserId';

export class SetUserId implements Action {
  readonly type = SET_USER_ID;
  constructor(public payload: string) {}
}

export class SetUserName implements Action {
  readonly type = SET_USER_NAME;
  constructor(public payload: string) {}
}

export class SetUserRole implements Action {
  readonly type = SET_USER_ROLE;
  constructor(public payload: string) {}
}

export class SetUserToken implements Action {
  readonly type = SET_USER_TOKEN;
  constructor(public payload: string) {}
}

export class SetIsLoggedIn implements Action {
  readonly type = SET_USER_LOGGED_IN;
  constructor(public payload: boolean) {}
}


export type UserActions =
SetUserName |
SetUserRole |
SetUserToken |
SetIsLoggedIn |
SetUserId;
