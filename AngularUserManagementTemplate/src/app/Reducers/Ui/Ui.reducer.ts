import { Action } from '@ngrx/store';
import { ModalModel } from '../../Models/Modal/modal-model';

import { UiActions, SET_MODAL } from 'src/app/Reducers/Ui/Ui.actions';

export interface UiState {
  Modal: ModalModel;
  //UserRole: string;
}

const initialState: UiState = {
  Modal: null,
  //UserRole: '',
};

export function UiReducer(state = initialState, action: UiActions) {
  switch (action.type) {
    case SET_MODAL:
      return {
        ...state,
        Modal: action.payload,
      };
    default: {
      return state;
    }
  }
}

export const GetModal = (state: UiState) => state.Modal;
//export const GetUserRole = (state: State) => state.UserRole;
