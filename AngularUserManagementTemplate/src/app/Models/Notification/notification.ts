export class Notification {

    constructor(){}
    NotificationId : number;
    UserId : number;
    GroupName : string;
    NotificationSubject : string;
    NotificationText : string;
    NotificationSend : Date;
    NotificationRead : Date;
    IsRead : boolean;

}
