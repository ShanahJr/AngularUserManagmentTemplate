import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { JwtHelperService } from '@auth0/angular-jwt';
import { OverlayService } from 'src/app/Services/Overlay/overlay.service';

import { UserService, TOKEN_NAME } from '../../../Services/User/user.service';
import { LogInModel } from '../../../Models/LogIn/log-in-model';
import { take } from 'rxjs/operators';
import { UserModel } from 'src/app/Models/User/user-model';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import * as Bootstrap from '../../../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import { ModalModel } from 'src/app/Models/Modal/modal-model';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
})
export class LogInComponent implements OnInit {
  model: any = {};
  errorMessage: string = '';
  UserID = 0;

  LoginForm = new FormGroup({
    userEmail: new FormControl('', [Validators.required, Validators.email]),
    userPassword: new FormControl('', [Validators.required]),
  });

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private overlayService: OverlayService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.UserID = parseInt(params.get('id'));
    });

    if (this.UserID > 0) {
      this.userService.ConfirmEmail(this.UserID);
    }
  } // ngOnInit

  LogIn() {
    var LogIn: LogInModel = this.LoginForm.value;
    this.userService.Login(LogIn)

  } // Login Method

  ForgotPassword() {
    console.log('Cheese');

    var modal = new ModalModel();
    modal.modalHeader =
      'You will get an email to confirm password reset, link expires in two hours';
    modal.modalMode = 'Forgot Password';
    modal.modalSubMode = '';
    this.userService.open(modal);
  }
}
