import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/Services/Notifcations/notification.service';
import { Notification } from '../../Models/Notification/notification'

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit ,AfterViewInit {

  NotificationSubscription : Subscription;
  NewNotification : Notification;

  ConnectionEstablishedSub : Subscription;

  constructor(
    private notificationService : NotificationService
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(){

    this.notificationService.connect();

    this.ConnectionEstablishedSub = this.notificationService.connectionEstablished
      .subscribe((isEstablished) => {

        if (isEstablished) {
          this.notificationService.AddToGroup();
        }

      })

    // this.notificationService.connect().then(() => {
    //   this.notificationService.AddToGroup();
    // });

    this.NotificationSubscription = this.notificationService.Notification$
      .subscribe( (notification) => {
        this.NewNotification = notification;

      })

  }

}
