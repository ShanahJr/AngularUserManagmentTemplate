import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/Services/Notifcations/notification.service';
import { Notification } from '../../Models/Notification/notification'

import * as fromRoot from "../../app.reducer";
import * as fromUser from "../../Reducers/User/User.reducer"
import { UserService } from '../../Services/User/user.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit, AfterViewInit {

  //Stores the Admin's Id so we cna know who sent out the notification
  ActiveUserId$ : Observable<string>;

  //Boolean values to contain
  ShowAllUsers : Boolean = false;
  MultiSelectUsers : Boolean = false;
  GroupSelect: Boolean = false;

  public CurrentNotification = new Notification();

  constructor(
    private notificationService : NotificationService,
    private UserState : Store<fromUser.UserState>,
    private userService : UserService
  ) {

  }

  ngOnInit(){
    this.ActiveUserId$ = this.UserState.select( fromRoot.GetUserId );
  }

  SendNotification(){

    // var notification  = new Notification();
    // notification.NotificationText = "Testing Notifications";
    this.ActiveUserId$.pipe(take(1)).subscribe((UserId => {
      this.CurrentNotification.UserId = parseInt(UserId);

    }))
    this.notificationService.connect();
    this.notificationService.Send(this.CurrentNotification);

  }

  OnNotificationSelectorChange(value : string)
  {
    console.log(value);
  }

  ngAfterViewInit(){
    this.notificationService.connect();

  }

}
