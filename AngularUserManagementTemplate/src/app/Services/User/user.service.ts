import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
// import * as jwt_decode from 'jwt-decode';
// import { JwtModule } from '@auth0/angular-jwt';
import { JwtHelperService } from '@auth0/angular-jwt';

import { LogInModel } from '../../Models/LogIn/log-in-model';
import { UserModel } from '../../Models/User/user-model';
import { take } from 'rxjs/operators';
import * as JwtDecode from 'jwt-decode';
import { ForgotPasswordModel } from 'src/app/Models/LogIn/forgot-password';

import { Store } from "@ngrx/store";
import * as UserActions from '../../Reducers/User/user.actions';
import * as fromUser from '../../Reducers/User/User.reducer';
import { environment } from 'src/environments/environment';
import { OverlayService } from '../Overlay/overlay.service';
import { ModalModel } from 'src/app/Models/Modal/modal-model';
import { NotificationService } from '../Notifcations/notification.service';

export const TOKEN_NAME: string = 'UserToken';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  url: string;
  token: string;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  httpOptions: any;

  model: any = {};
  errorMessage: string = '';
  UserID = 0;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private overlayService: OverlayService,
    private UserState : Store<fromUser.UserState>,
    private notificationService : NotificationService
    ) {

    //this.url = 'https://localhost:6002/api/User/';
    let headers = new HttpHeaders({
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    });
    let option = { headers: headers };
    // this.url = 'https://projectmanagerapi.shanahjr.co.za/api/User/';
    this.url = 'https://localhost:6002/api/User/';

  } // constructor


  //If no value is returned then it wasa success
  Login(model: LogInModel) {

    this.http.post<any>(environment.ApiUrl + 'User/Login', model)
    .pipe(take(1))
      .subscribe(
        (res) => {
          if (res.token) {
            localStorage.setItem(TOKEN_NAME, res.token);
            const helper = new JwtHelperService();
            const decodedToken = helper.decodeToken(
              localStorage.getItem(TOKEN_NAME)
            );
            localStorage.setItem('UserRole', decodedToken.Role);

            this.UserState.dispatch( new UserActions.SetUserName(res.userName));
            this.UserState.dispatch( new UserActions.SetUserRole(decodedToken.Role));
            this.UserState.dispatch( new UserActions.SetUserToken(res.token) );
            this.UserState.dispatch( new UserActions.SetIsLoggedIn(true) );
            this.UserState.dispatch( new UserActions.SetUserId(decodedToken.UserId));

            if (decodedToken.Role == "General") {
              this.router.navigate(['/Projects']);
            }
            else
            {
              this.router.navigate(['/AdminDashboard'])
            }

          }
        },
        (error) => {
          if (error.error == 'Unauthorized') {
            var modal = new ModalModel();
            modal.modalHeader = 'Oops!';
            modal.modalBody =
              'Your email or password is incorrect. Please check them and try again';
            modal.modalMode = 'Normal Message';
            this.open(modal);
          } else if (error.error == 'You have not verified your email yet') {
            var modal = new ModalModel();
            modal.modalHeader = 'There is a slight problem';
            modal.modalBody =
              'You need to have confirmed your email in order to logIn.';
            modal.modalMode = 'Normal Message';
            modal.modalSubMode = 'Confirm Email';
            this.open(modal);
          } else {
            var modal = new ModalModel();
            modal.modalHeader = 'We are very sorry.';
            modal.modalBody =
              'Our servers are down and we are working on getting them back up, please try logging in later';
            modal.modalMode = 'Normal Message';
            this.open(modal);
          }
        }
      );;

  } //LogIn

  ConfirmEmail(id: number) {
    this.http.get(environment.ApiUrl + 'User/ConfirmEmail/' + id)
    .pipe(take(1))
        .subscribe(
          () => {
            var modal = new ModalModel();
            modal.modalHeader = 'Hooorayyyyy!!!!';
            modal.modalBody =
              'Your account has been verified. We hope you enjoy the experience.';
            modal.modalMode = 'Normal Message';
            this.open(modal);
          },
          (error) => {
            if (error.statusText == 'Unknown Error') {
              var modal = new ModalModel();
              modal.modalHeader = 'Oh No!';
              modal.modalBody =
                'Something is wrong with our servers, we cant verify your account at the moment. Please come back and try again later.';
              modal.modalMode = 'Normal Message';
              this.open(modal);
            }
            if (error.statusText == 'Conflict') {
              var modal = new ModalModel();
              modal.modalHeader = 'This must be a mistake';
              modal.modalBody =
                'You have aready confirmed your email address, you are all set to log in.';
              modal.modalMode = 'Normal Message';
              this.open(modal);
            }
          }
        );;
  }

  Logout() {
    this.UserState.dispatch( new UserActions.SetUserName(""));
    this.UserState.dispatch( new UserActions.SetUserRole(""));
    this.UserState.dispatch( new UserActions.SetUserToken("") );
    this.UserState.dispatch( new UserActions.SetIsLoggedIn(false) );
    this.router.navigate(['/LogIn']);
    this.notificationService.RemoveFromGroup();
  }

  Register(model: UserModel) {
    return this.http.post<UserModel>(this.url, model, {
      headers: this.headers,
    });
  } //Register





















  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }



  ResendConfirmationEmail(email: string) {
    return this.http.get(this.url + 'ResendConfirmationEmail/' + email);
  }

  ForgotPassword(email: string) {
    return this.http.get(this.url + 'ForgotPassword/' + email);
  }

  ChangePassword(Password: string, token: string) {
    //let info = { Password: Password };
    var newPassword = new ForgotPasswordModel();
    newPassword.password = Password;
    //this.httpOptions.headers.set('Authorization', 'bearer ' + token);
    let headers = new HttpHeaders({
      Authorization: 'bearer ' + token,
    });
    let option = { headers: headers };
    return this.http.post(this.url + 'ChangePassword/', newPassword, option);
  }

  //Method to open the global modal
  open(modal: ModalModel) {
    const ref = this.overlayService.open(modal, null);

    ref.afterClosed$.subscribe((res) => {});
  }

  // getTokenExpirationDate(token: string): Date {
  //   const helper = new JwtHelperService();

  //   const expirationDate = helper.getTokenExpirationDate(token);

  //   if (expirationDate === undefined) return null;

  //   return expirationDate;
  // }

  // isTokenExpired(token?: string): boolean {
  //   if (!token) token = this.getToken();
  //   if (!token) return true;

  //   const date = this.getTokenExpirationDate(token);
  //   if (date === undefined) return false;
  //   return !(date.valueOf() > new Date().valueOf());
  // }
} // Export
