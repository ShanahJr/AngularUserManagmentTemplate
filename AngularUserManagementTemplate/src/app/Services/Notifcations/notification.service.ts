import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Notification } from '../../Models/Notification/notification'
import { UserService } from '../User/user.service';

import * as fromRoot from "../../app.reducer";
import * as fromUser from "../../Reducers/User/User.reducer"
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private connection: signalR.HubConnection;
  connectionEstablished = new Subject<Boolean>();
  Notification$  = new Subject<Notification>();

  private ActiveUserRole$ : Observable<string>;

  constructor(
    private UserState : Store<fromUser.UserState>,
  ) {}

  connect() {
    // return Promise.resolve((() => {

    //   if (!this.connection) {
    //     this.connection = new signalR.HubConnectionBuilder()
    //     .withUrl('https://localhost:6002' +
    //         '/Notification',{
    //           skipNegotiation : true,
    //           transport : signalR.HttpTransportType.WebSockets
    //         })
    //     .build();

    //     this.connection.start().then(() => {
    //       console.log('Hub connection started');
    //       this.connectionEstablished.next(true);
    //     }).catch(err =>{
    //       debugger;
    //         console.log(err)
    //       });

    //     this.connection.on('NotificationToAll', (notification : Notification) => {
    //       debugger;
    //       console.log('Received', notification);
    //       this.Notification$.next(notification);
    //     });


    //    }

    //     return true; // return whatever you want not neccessory
    // })());
    if (!this.connection) {
      this.connection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:6002' +
          '/Notification',{
            skipNegotiation : true,
            transport : signalR.HttpTransportType.WebSockets
          })
      .build();

      this.connection.start().then(() => {
        console.log('Hub connection started');
        this.connectionEstablished.next(true);
      }).catch(err =>{
        debugger;
          console.log(err)
        });

     }
     else{
      this.connectionEstablished.next(true);
     }
     this.connection.on('NotificationToAll', (notification : Notification) => {
      console.log('Received', notification);
      this.Notification$.next(notification);
    });

  }// End of Connect

  Send( notification : Notification ){
    notification.GroupName = "AllUsersNotification";
    this.connection.send("newNotification" , notification)
  }

  AddToGroup(){
    this.ActiveUserRole$ = this.UserState.select(fromRoot.GetUserRole);

    this.ActiveUserRole$.pipe(take(1)).subscribe((userRole) => {

      if (userRole != "Admin") {
        this.connection.send("AddToGroup", "AllUsersNotification")
      }

    })

  }

  RemoveFromGroup(){
    this.ActiveUserRole$.pipe(take(1)).subscribe((userRole) => {

      if (userRole != "Admin") {
        this.connection.send("RemoveFromGroup", "AllUsersNotification")
      }

    })
  }

  disconnect() {
    if (this.connection) {
      this.connection.stop();
      this.connection = null;
    }
  }


}
