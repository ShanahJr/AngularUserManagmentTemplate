import { Injectable, OnInit } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import * as fromRoot from "../../app.reducer";
import * as fromUser from "../../Reducers/User/User.reducer"

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, OnInit {

  ActiveUserRole$ : Observable<string>;
  ActiveUserToken$ : Observable<string>;

  constructor(
    private router: Router,
    private UserState : Store<fromUser.UserState>
  ) {}

  ngOnInit(){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {

    this.ActiveUserRole$ = this.UserState.select( fromRoot.GetUserRole );
    this.ActiveUserToken$ = this.UserState.select(fromRoot.GetUserToken);

    this.ActiveUserToken$.pipe(take(1)).subscribe( (token) => {

      if (token == null || token == undefined || token == "") {
        this.router.navigate(['/LogIn']);
      }
      else
      {
        let role = next.data['role'] as string;

        this.ActiveUserRole$.pipe(take(1)).subscribe( (role) => {
          if (role) {
            if (role == role) {
              return true;
            } else {
              this.router.navigate(['/Forbidden']);
            }
          }
          
        })
      }
    })
    return true;

  }
}
